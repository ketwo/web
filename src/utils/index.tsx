export const flatten = (obj: { [key: string]: any }, parent: string = '', res: { [key: string]: any } = {}) => {
  for (let key in obj) {
    let propName = parent ? parent + '-' + key : key;
    if(typeof obj[key] == 'object') {
      flatten(obj[key], propName, res);
    } else {
      res[propName] = obj[key];
    }
  }
  return res;
};