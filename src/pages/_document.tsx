import React from 'react';
import Document, { Html, Head, Main, NextScript } from 'next/document';
import * as Theme from 'hooks/theme';



class MyDocument extends Document {
  render() {
    return (
      <Html>
        <Head />
        <Theme.Script themes={Theme.THEMES} defaultTheme={Theme.DEFAULT} />
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument