import type { AppProps } from 'next/app';
import React from 'react';
import * as Settings from 'hooks/settings';
import * as Theme from 'hooks/theme';

import 'styles/global.sass';

const MyApp: React.FC<AppProps> = ({ Component, pageProps }) => {
  // const settingsContext = Settings.context;
  const themeContext = Theme.context;
  return (
    <Component {...pageProps} />
  );
}

export default MyApp