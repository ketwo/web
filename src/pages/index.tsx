import { Board } from "components/board";
import { Skeleton } from "components/skeleton";
import { Ui } from "components/ui";
import { AnimatePresence, motion } from "framer-motion";
import React, { useEffect, useState } from "react";

type Props = {

};
const Index: React.FC<Props> = () => {
  const [loading, setLoading] = useState<boolean>(true);
  useEffect(() => {
    console.log('?');
    window.addEventListener('dblclick', () => setLoading(false));
    // setLoading(false);
  }, []);
  return (
    <AnimatePresence exitBeforeEnter>
      {
        loading ? (
          <Skeleton key='skeleton' duration={0.8} ease={[0.5, 1, 0.89, 1]} />
        ) : (
          <Ui key='ui'/>
        )
      }
  </AnimatePresence>
  );
};

export default Index;