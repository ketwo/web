export type Settings = {
  sound: {
    enabled: boolean
  },
  animations: {
    enabled: boolean
  }
};
export const DEFAULT = {
  sound: {
    enabled: true,
  },
  animations: {
    enabled: true
  }
};
export const get: () => Settings = () => {
  if (typeof window === 'undefined') {
    return DEFAULT;
  }
  return (window as any).settings;
};