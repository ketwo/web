import React from "react";
import * as Settings from "./settings";

export type Context = Settings.Settings;
export const context = React.createContext<Context>(Settings.get());