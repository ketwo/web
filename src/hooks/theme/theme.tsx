export type Theme = {
  fill: {
    primary: string,
    secondary: string,
    tertiary: string
  },
  font: {
    color: {
      primary: string,
      secondary: string
    }
  }
};
export const THEMES: { [key: string]: Theme } = {
  'ketwo:dark': {
    fill: {
      primary: '#542966',
      secondary: '#93E1D8',
      tertiary: '#B95071'
    },
    font: {
      color: {
        primary: '',
        secondary: '',
      }
    }
  }
}
export const DEFAULT = 'ketwo:dark';
export const get: () => Theme = () => {
  if (typeof window === 'undefined') {
    return THEMES[DEFAULT];
  }
  return THEMES[(window as any).theme];
};