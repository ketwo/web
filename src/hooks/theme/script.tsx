import { flatten } from 'utils';
import { Theme } from './theme';

export type Props = {
  defaultTheme: string,
  themes: { [key: string]: Theme },
  storageKey?: string
  scriptClass?: string
};
export const Script: React.FC<Props> = ({ 
    defaultTheme, themes, storageKey = 'theme', scriptClass = 'theme'
  }) => {
  const flat = JSON.stringify(Object.keys(themes).reduce((prev: { [key: string]: any}, key: string) => {
    prev[key] = flatten(themes[key], '--theme');
    return prev;
  }, {}));
  
  const script = `
  (() => {
    const STORAGE_KEY = '${storageKey}';
    const DEFAULT_THEME = '${defaultTheme}';
    const THEMES = JSON.parse('${flat}');
  
    let name = localStorage.getItem(STORAGE_KEY) ?? DEFAULT_THEME;
    let theme = THEMES[name] ?? THEMES[DEFAULT_THEME];
    localStorage.setItem(STORAGE_KEY, name);

    window.theme = name;
    const root = document.documentElement;
  
    for (const [key, value] of Object.entries(theme)) {
      root.style.setProperty(key, value);
    }
  })();`;
  
  return (
    <script dangerouslySetInnerHTML={{
      __html: script
    }} />
  );
};