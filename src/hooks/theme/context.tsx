import React from "react";
import * as Theme from "./theme";

export type Context = Theme.Theme;
export const context = React.createContext<Context>(Theme.get());