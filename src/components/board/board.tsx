import React, { useRef } from "react";
import styles from './styles/index.module.sass';

type Props = {
  orientation: 'WHITE'|'BLACK',
};
export const Board: React.FC<Props> = ({ orientation = 'WHITE' }) => {
  const ref = useRef<HTMLTableSectionElement>(null);
  return (
    <section ref={ref} className={styles.root}>
      {/* <svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
        { 
          Array.from({ length: 64 }, (_, i) => {
            return (
              <rect x={0 } y={0} width="12.5%" height="12.5%" fill="red" />
            );
          })
        }
      </svg> */}
    </section>
  );
};