import { motion } from 'framer-motion';
import React from 'react';
import { Logo } from './components/logo';
import styles from './styles/index.module.sass';



export type Props = {
  duration: number,
  ease: number[]
};
export const Skeleton: React.FC<Props> = ({ duration, ease }) => {
  const variants = {
    background: {
      initial: {
        x: 0
      },
      exit: {
        x: '-100%',
        transition: {
          delay: duration,
          duration,
          ease,
        }
      }
    }
  };
  return (
    <div
      className={styles.root}
    >
      <svg 
        xmlns="http://www.w3.org/2000/svg"
        className={styles.background}
      >
        <motion.rect 
          width='100%' 
          height='100%' 

          variants={variants.background}
          animate={true}
          initial='initial'
          exit='exit'
        />
      </svg>

      <Logo duration={duration} ease={ease} />
    </div>
  );
};