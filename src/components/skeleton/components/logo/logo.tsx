import { motion } from "framer-motion";
import styles from './styles/index.module.sass';

export type Props = {
  ease: number[],
  duration: number
};
export const Logo: React.FC<Props> = ({ ease, duration }) => {
  const variants = {
    root: {
      initial: {
        clipPath: 'polygon(0% 0%, 100% 0%, 100% 100%, 0% 100%)'
      },
      exit: {
        clipPath: 'polygon(0% 0%, 0% 0%, 0% 100%, 0% 100%)',
        transition: {
          delay: duration/2,
          duration: duration/2,
          ease
        }
      }
    },
    cover: {  
      initial: {
        x: '100%'
      },
      exit: {
        x: '0%',
        transition: {
          duration: duration/2,
          ease
        }
      }
    }
  };

  return (
    <motion.svg 
      viewBox="0 0 123 89"
      xmlns="http://www.w3.org/2000/svg"
      className={styles.root}

      variants={variants.root}
      animate={true}
      initial='initial'
      exit='exit'
    >
      <path d="M8.449 73H17.921V59.688L24.129 51.688L36.417 73H46.785L29.761 44.264L44.225 25.576H33.793L18.113 46.184H17.921V25.576H8.449V73ZM66.172 73.896C70.588 73.896 75.132 72.36 78.652 69.928L75.452 64.168C72.892 65.768 70.332 66.664 67.388 66.664C62.076 66.664 58.236 63.592 57.468 57.704H79.548C79.804 56.872 79.996 55.144 79.996 53.352C79.996 43.496 74.876 36.264 65.02 36.264C56.508 36.264 48.316 43.496 48.316 55.08C48.316 66.92 56.124 73.896 66.172 73.896ZM57.34 51.432C58.044 46.248 61.372 43.56 65.148 43.56C69.756 43.56 71.932 46.632 71.932 51.432H57.34ZM85.4395 73H117.184V65.064H106.816C104.576 65.064 101.44 65.32 99.0715 65.576C107.52 56.744 114.944 48.424 114.944 40.36C114.944 31.656 108.992 25.96 100.032 25.96C93.5675 25.96 89.3435 28.52 84.9915 33.192L90.1115 38.312C92.5435 35.688 95.3595 33.384 98.8155 33.384C103.36 33.384 105.92 36.2 105.92 40.872C105.92 47.72 97.7915 55.784 85.4395 67.56V73Z" />
      <motion.rect
        width='100%' 
        height='100%'

        variants={variants.cover}
        animate={true}
        initial='initial'
        exit='exit'
      />
    </motion.svg>
  );
};