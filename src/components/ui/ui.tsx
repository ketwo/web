import { motion } from "framer-motion";

const variants = {
  initial: {
    opacity: 0
  },
  animate: {
    opacity: 1,
    transition: {
      duration: 1
    }
  }
};

export type Props = {

};
export const Ui: React.FC<Props> = () => {
  return (
    <motion.h1
      variants={variants}
      initial='initial'
      animate='animate'
    >
      ui
    </motion.h1>
  );
};