const path = require('path')

module.exports = {
  webpack: (config) => {
    config.output = Object.assign(config.output, {
      webassemblyModuleFilename: 'static/wasm/[modulehash].wasm'
    })

    return config;
  },
  sassOptions: {
    includePaths: [path.join(__dirname, './src/styles')],
  },
  devIndicators: {
    autoPrerender: false,
  },
};